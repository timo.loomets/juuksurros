#include <ros/ros.h>
#include <math.h>
#include <std_msgs/UInt16.h>
#include <std_msgs/Float32.h>
#include <std_msgs/Bool.h>
#include <std_msgs/Int32.h>

#define MAX_SPEED 30.0f
#define MAX_ACC 200.f // cm/s^2
#define THROTTLE_INITIAL 1000 // estimated hovering value
#define THROTTLE_GAIN 300.0f
#define MAX_THROTTLE 1555.0f
#define DESCENT_SPEED 30.0f

#define AUTO_MODE_MANUAL 0
#define AUTO_MODE_FLY 1
#define AUTO_MODE_LAND 2

#define CLAMP(x, low, high)  (((x) > (high)) ? (high) : (((x) < (low)) ? (low) : (x)))

static float pitch;
static float roll;
static float accZ;
static int32_t ctrlMode;

static int heightId;
static float lastHeight;
static float height;
static ros::Time heightTime; 
static ros::Time lastHeightTime; 

void pitchCb(const std_msgs::Float32 msg) {
    pitch = msg.data;
}

void rollCb(const std_msgs::Float32 msg) {
    roll = msg.data;
}

void rangeCb(const std_msgs::Float32 msg) {
    lastHeight = height;
    lastHeightTime = heightTime;
    height = (float)msg.data;
    heightTime = ros::Time::now();
    heightId++;
}

void accZCb(const std_msgs::Float32 msg) {
    accZ = msg.data*100.0f; // control code uses cm/s
    ROS_INFO("accZ %f...\r\n", accZ);
}

void ctrlModeCb(const std_msgs::Int32 msg) {
    ctrlMode = msg.data;
}

int main(int argc, char **argv) {
    ros::init(argc, argv, "hover");
    ros::NodeHandle n("~");
    ros::Rate loop_rate(50);

    ROS_INFO("starting hover_node...\r\n");

    ctrlMode = AUTO_MODE_MANUAL;

    height = 30.0f;
    heightTime = ros::Time::now();
    lastHeightTime = ros::Time::now();

    // latch - true
    ros::Publisher armPub = n.advertise<std_msgs::UInt16>("/control/arm", 1, true);
    ros::Publisher yawPub = n.advertise<std_msgs::UInt16>("/control/yaw", 1, true);
    ros::Publisher pitchPub = n.advertise<std_msgs::UInt16>("/control/pitch", 1, true);
    ros::Publisher rollPub = n.advertise<std_msgs::UInt16>("/control/roll", 1, true);
    ros::Publisher throttlePub = n.advertise<std_msgs::UInt16>("/control/throttle", 1, true);

    ros::Subscriber telPitchSub = n.subscribe<std_msgs::Float32>("/telemetry/pitch", 1, pitchCb);
    ros::Subscriber telRollSub = n.subscribe<std_msgs::Float32>("/telemetry/roll", 1, rollCb);
    ros::Subscriber telRangeSub = n.subscribe<std_msgs::Float32>("/telemetry/range", 1, rangeCb);
    ros::Subscriber telAccZSub = n.subscribe<std_msgs::Float32>("/telemetry/accZ", 1, accZCb);
    ros::Subscriber telControlMode = n.subscribe<std_msgs::Int32>("/telemetry/controlmode", 1, ctrlModeCb);

    std_msgs::UInt16 v;
    v.data = 2000;
    armPub.publish(v);
    v.data = 1500;
    yawPub.publish(v);
    //rollPub.publish(v);
    //pitchPub.publish(v);
    v.data = 0;
    throttlePub.publish(v);

    while(ros::ok()) {
        ros::spinOnce();

        float holdHeight;
        float curThrottle;
        ros::Time lastTime;
        ros::Time curTime;
        if(ctrlMode > AUTO_MODE_MANUAL) {
            if(ctrlMode == AUTO_MODE_FLY) {
                holdHeight = 100.0f;
            } else if(ctrlMode == AUTO_MODE_LAND){
                holdHeight = 0.0f;
            }
            curThrottle = THROTTLE_INITIAL;
            curTime = lastTime = ros::Time::now();
            v.data = 1500;
            yawPub.publish(v);
            //rollPub.publish(v);
            //pitchPub.publish(v);
            v.data = 0;
            throttlePub.publish(v);
        }

        while(ros::ok() && ctrlMode > AUTO_MODE_MANUAL) {
            ros::spinOnce();

            //float realRange = range * cosf((pitch/180.0f)*M_PI) * cosf((roll/180.0f)*M_PI);
            float realHeight = height;

            curTime = ros::Time::now();
            auto dt = (curTime - lastTime).toSec();

            if(ctrlMode == AUTO_MODE_FLY) { // autonomous mode
                holdHeight = 100.0f;
            } else if(ctrlMode == AUTO_MODE_LAND) { // landing mode
                //holdHeight -= DESCENT_SPEED*dt;
                //holdHeight = CLAMP(holdHeight, 0.0f, 1000.0f);
		holdHeight = 0.0f;
            }
	    ROS_INFO("Holdheight %f\r\n", holdHeight);
            
            // TODO: average height over past iterations
            float verticalSpeed = (realHeight - lastHeight) / (float)((heightTime-lastHeightTime).toSec());

            float heightError = holdHeight - realHeight;
            float heightErrorScalar = CLAMP(heightError / realHeight, -1.0f, 1.0f);

            float speedTarget = MAX_SPEED * heightErrorScalar;
            float speedError = speedTarget - verticalSpeed;
            float speedErrorScalar = CLAMP(speedError / MAX_SPEED, -1.0f, 1.0f);
            float accTarget = (MAX_ACC * speedErrorScalar) + 995.0f;
            float accError = accTarget - accZ;
            float finalScalar = accError / MAX_ACC;

            float throttleChange = ((float)THROTTLE_GAIN*finalScalar*dt);
            curThrottle += throttleChange;
            curThrottle = CLAMP(curThrottle, 1000.0f, MAX_THROTTLE);
            lastHeight = height;
        
            //ROS_INFO("t %f hE %f spdE %f accE %f speed %f accZ %f final %f mode: %d\r\n", curThrottle, heightError, speedError, accError, verticalSpeed, accZ, finalScalar, ctrlMode);

            v.data = (uint16_t)curThrottle;
            throttlePub.publish(v);
            lastTime = curTime;

            loop_rate.sleep();
        }
        loop_rate.sleep();
    }
}
